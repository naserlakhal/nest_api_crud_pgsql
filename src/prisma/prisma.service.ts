import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient {
  constructor(config: ConfigService) {
    super({
      datasources: {
        db: {
          // i got an error here and i solve it by npx prisma generate
          // url: 'postgresql://postgres:admin@127.0.0.1:5432/nestTutorial',
          url: config.get('DATABASE_URL'),
        },
      },
    });
    console.log('checking the error where', config.get('DATABASE_URL'));
  }
}
